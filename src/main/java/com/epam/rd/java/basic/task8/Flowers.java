package com.epam.rd.java.basic.task8.model;

import java.util.List;

public class Flowers {

    private List<com.epam.rd.java.basic.task8.model.Flower> flowerList;

    private String root;

    public Flowers() {
    }

    public Flowers(List<com.epam.rd.java.basic.task8.model.Flower> flowerList, String root) {
        this.flowerList = flowerList;
        this.root = root;
    }

    public List<com.epam.rd.java.basic.task8.model.Flower> getFlowerList() {
        return flowerList;
    }

    public String getRoot() {
        return root;
    }

    public void setRoot(String root) {
        this.root = root;
    }

    public void setFlowerList(List<com.epam.rd.java.basic.task8.model.Flower> flowerList) {
        this.flowerList = flowerList;
    }

    @Override
    public String toString() {
        return getRoot()+"{" +
                "flowerList=" + flowerList +
                "}\n";
    }
}
